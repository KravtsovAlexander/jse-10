package ru.t1.kravtsov.tm.controller;

import ru.t1.kravtsov.tm.api.controller.ICommandController;
import ru.t1.kravtsov.tm.api.service.ICommandService;
import ru.t1.kravtsov.tm.model.Command;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    @Override
    public void displayHelp() {
        System.out.println("[HELP]");
        for (Command command : commandService.getTerminalCommands()) {
            System.out.println(command);
        }
    }

    @Override
    public void displayArguments() {
        System.out.println("[ARGUMENTS]");
        for (Command command : commandService.getTerminalCommands()) {
            displayNullableString(command.getArgument());
        }
    }

    @Override
    public void displayCommands() {
        System.out.println("[COMMANDS]");
        for (Command command : commandService.getTerminalCommands()) {
            displayNullableString(command.getName());
        }
    }

    private void displayNullableString(final String str) {
        if (str == null || str.isEmpty()) return;

        System.out.println(str);
    }

    @Override
    public void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.10.0");
    }

    @Override
    public void displayAbout() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Alexander Kravtsov");
        System.out.println("EMAIL: aekravtsov@nota.tech");
    }

    @Override
    public void displayArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported");
    }

    @Override
    public void displayCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported");
    }

}
