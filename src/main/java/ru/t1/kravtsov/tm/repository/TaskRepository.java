package ru.t1.kravtsov.tm.repository;

import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.model.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    public TaskRepository() {
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void deleteAll() {
        tasks.clear();
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index < 0 || index >= tasks.size()) return null;
        return tasks.get(index);
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        return remove(task);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        return remove(task);
    }

    @Override
    public List<Task> removeByName(String name) {
        final List<Task> removedTasks = new ArrayList<>();
        final Iterator<Task> iterator = findAll().iterator();
        while (iterator.hasNext()) {
            final Task task = iterator.next();
            if (name.equals(task.getName())) {
                iterator.remove();
                removedTasks.add(task);
            }
        }

        return removedTasks;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) {
                result.add(task);
            }
        }
        return result;
    }

}
