package ru.t1.kravtsov.tm.repository;

import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.model.Project;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    public ProjectRepository() {
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void deleteAll() {
        projects.clear();
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index < 0 || index >= projects.size()) return null;
        return projects.get(index);
    }

    @Override
    public Project remove(final Project project) {
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        return remove(project);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        return remove(project);
    }

    @Override
    public List<Project> removeByName(String name) {
        final List<Project> removedProjects = new ArrayList<>();
        final Iterator<Project> iterator = findAll().iterator();
        while (iterator.hasNext()) {
            final Project project = iterator.next();
            if (name.equals(project.getName())) {
                iterator.remove();
                removedProjects.add(project);
            }
        }

        return removedProjects;
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

}
